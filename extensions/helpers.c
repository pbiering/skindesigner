#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "helpers.h"
#include <vdr/skins.h>

cPlugin *GetScraperPlugin(void) {
    static cPlugin *pScraper = cPluginManager::GetPlugin("scraper2vdr");
    if( !pScraper ) // if it doesn't exit, try tvscraper
        pScraper = cPluginManager::GetPlugin("tvscraper");
    return pScraper;
}

cSize ScaleToFit(int widthMax, int heightMax, int widthOriginal, int heightOriginal) {
    int width = 1;
    int height = 1;

    if ((widthMax == 0)||(heightMax==0)||(widthOriginal==0)||(heightOriginal==0))
        return cSize(width, height);

    if ((widthOriginal <= widthMax) && (heightOriginal <= heightMax)) {
        width = widthOriginal;
        height = heightOriginal;
    } else if ((widthOriginal > widthMax) && (heightOriginal <= heightMax)) {
        width = widthMax;
        height = (double)width/(double)widthOriginal * heightOriginal;
    } else if ((widthOriginal <= widthMax) && (heightOriginal > heightMax)) {
        height = heightMax;
        width = (double)height/(double)heightOriginal * widthOriginal;
    } else {
        width = widthMax;
        height = (double)width/(double)widthOriginal * heightOriginal;
        if (height > heightMax) {
            height = heightMax;
            width = (double)height/(double)heightOriginal * widthOriginal;
        }
    }
    return cSize(width, height);
}

int Minimum(int a, int b, int c, int d, int e, int f) {
    int min = a;
    if (b < min) min = b;
    if (c < min) min = c;
    if (d < min) min = d;
    if (e < min) min = e;
    if (f < min) min = f;
    return min;
}

string StrToLowerCase(string str) {
    string lowerCase = str;
    const int length = lowerCase.length();
    for(int i=0; i < length; ++i) {
        lowerCase[i] = std::tolower(lowerCase[i]);
    }
    return lowerCase;
}

bool isNumber(const string& s) {
    string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

bool IsToken(const string& token) {
    if ((token.find("{") == 0) && (token.find("}") == (token.size()-1)))
        return true;
    return false;
}

bool FileExists(const string &fullpath) {
    struct stat buffer;
    if (stat (fullpath.c_str(), &buffer) == 0) {
        return true;
    }
    if (config.debugImageLoading) {
        dsyslog("skindesigner: did not find %s", fullpath.c_str());
    }
    return false;
}

bool FileExists(const string &path, const string &name, const string &ext) {
    stringstream fileName;
    fileName << path << name << "." << ext;
    struct stat buffer;
    if (stat (fileName.str().c_str(), &buffer) == 0) {
        return true;
    }
    if (config.debugImageLoading) {
        dsyslog("skindesigner: did not find %s", fileName.str().c_str());
    }
    return false; 
}

bool FolderExists(const string &path) {
    struct stat buffer;
    return stat(path.c_str(), &buffer) == 0 && S_ISDIR(buffer.st_mode);
}

bool FirstFileInFolder(string &path, string &extension, string &fileName) {
    DIR *folder = NULL;
    struct dirent *file;
    folder = opendir(path.c_str());
    if (!folder)
        return false;
    while (file = readdir(folder)) {
        if (endswith(file->d_name, extension.c_str())) {
            string currentFileName = file->d_name;
            int strlength = currentFileName.size();
            if (strlength < 8)
                continue;
            fileName = currentFileName;
            return true;
        }
    }
    return false;
}

void CreateFolder(string &path) {
    cString command = cString::sprintf("mkdir -p %s", path.c_str());
    int ok = system(*command);
    if (!ok) {}
}

// trim from start
string &ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
    return s;
}

// trim from end
string &rtrim(string &s) {
    s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
    return s;
}

// trim from both ends
string &trim(string &s) {
    return ltrim(rtrim(s));
}

// split: receives a char delimiter; returns a vector of strings
// By default ignores repeated delimiters, unless argument rep == 1.
vector<string>& splitstring::split(char delim, int rep) {
    if (!flds.empty()) flds.clear();  // empty vector if necessary
    string work = data();
    string buf = "";
    int i = 0;
    while (i < (int)work.length()) {
        if (work[i] != delim)
            buf += work[i];
        else if (rep == 1) {
            flds.push_back(buf);
            buf = "";
        } else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}

cStopWatch::cStopWatch(const char* message) {
    start = cTimeMs::Now();
    last = start;
    if (message) {
        dsyslog("skindesigner: Starting StopWatch %s", message);        
    }
}

void cStopWatch::Report(const char* message) {
    dsyslog("skindesigner: %s - needed %d ms", message, (int)(cTimeMs::Now() - last));
    last = cTimeMs::Now();
}

void cStopWatch::Stop(const char* message) {
    dsyslog("skindesigner: %s - needed %d ms", message, (int)(cTimeMs::Now() - start));
}

string GetTimeString(int seconds) {
    time_t sec(seconds);
    tm *p = gmtime(&sec);
    int hours = p->tm_hour;
    int mins = p->tm_min;
    int secs = p->tm_sec;
    if (hours > 0) {
        return *cString::sprintf("%d:%02d:%02d", hours, mins, secs);
    }
    return *cString::sprintf("%02d:%02d", mins, secs);;
}


//View Helpers
string GetScreenResolutionString(int width, int height, bool *isHD, bool *isUHD) {
    // TODO: try to get more information from information sources about interlace/progressive
    // cDevice::PrimaryDevice()->GetVideoSize is NOT providing enough information
    *isHD = false; // default
    *isUHD = false; // default
    string name = "";
    switch (height) {
        case 4320: // 7680 x 4320 = 8K UHD
            name = "uhd4320p";
            *isHD = true;
            *isUHD = true;
            break;
        case 2160: // 3840 x 2160 = 4K UHD
            name  = "uhd2160p";
            *isHD = true;
            *isUHD = true;
            break;
        case 1440: // 2560 x 1440 = QHD
            name = "hd1440p";
            *isHD = true;
            break;
        case 1080:
            name = "hd1080i"; // 'i' is default, 'p' can't be detected currently
            *isHD = true;
            break;
        case 720:
            name = "hd720p"; // 'i' is not defined in standards
            *isHD = true;
            break;
        case 576:
            name = "sd576i"; // assumed 'i'
            break;
        case 480:
            name = "sd480i"; // assumed 'i'
            break;
        default:
            name = "unknown";
            break;
    }
    return name;
}

string GetScreenAspectString(double aspect, bool *isWideScreen) {
    string name = "";
    *isWideScreen = false;
    if (aspect == 4.0/3.0) {
        name = "4:3";
        *isWideScreen = false;
    } else if (aspect == 16.0/9.0) {
        name = "16:9";
        *isWideScreen = true;
    } else if (aspect == 2.21) {
        name = "21:9";
        *isWideScreen = true;
    }
    return name;
}

int RecordingTyp(const char *filename) {
    // detect Audio and Video from 'info'
    bool hasAudio = false;
    bool hasVideo = false;
    int format = SD;
    bool isHD = false;
    int type = -1;

    cComponents *Components = NULL;
    tChannelID channelID = tChannelID::InvalidID;
    double FramesPerSecond = 0;
    {
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
        // a longer LOCK is necessary to avoid a possible segfault in cComponents (epg.c)
        // if the video directory is on a disk that is currently in sleep mode
        LOCK_RECORDINGS_READ;
        const cRecordings* recordings = Recordings;
#else
        cRecordings* recordings = &Recordings;
#endif

        if (const cRecording *Recording = recordings->GetByName(filename)) {
            const cRecordingInfo *info = Recording->Info();
            FramesPerSecond = info->FramesPerSecond();
            channelID = info->ChannelID();
            Components = (cComponents *)info->Components();
        }

        if (Components) {
            // see also ETSI EN 300 468
            // Stream: 1 = MPEG2-Video, 2 = MPEG2 Audio, 3 = Untertitel, 4 = AC3-Audio, 5 = H.264-Video, 6 = HEAAC-Audio, 7 = DTS/DTS HD audio, 8 = SRM/CPCM data, 9 = HEVC Video, AC4 Audio

            // detect audio
            if (Components->GetComponent(0, 2, 0)) {        // recording info: "X 2 <type>"
                hasAudio = true;
            } else if (Components->GetComponent(0, 4, 0)) { // recording info: "X 4 <type>"
                hasAudio = true;
            } else if (Components->GetComponent(0, 6, 0)) { // recording info: "X 6 <type>"
                hasAudio = true;
            } else if (Components->GetComponent(0, 7, 0)) { // recording info: "X 7 <type>"
                hasAudio = true;
            };

            // detect Video / HD / UHD
            // Stream == Video(1|5): 01 = 05 = 4:3, 02 = 03 = 06 = 07 = 16:9, 04 = 08 = >16:9, 09 = 0D = HD 4:3, 0A = 0B = 0E = 0F = HD 16:9, 0C = 10 = HD >16:9
            // Stream: 9 = HEVC Video, AC4 Audio
            // Stream == Video(9): 00|01|02|03 = HD, 04|05|06|07 = UHD

            tComponent *Component = NULL;
            if (Component = Components->GetComponent(0, 9, 0)) {        // #1: HVEC (stream content: 9 - recording info: "X 9 <type>"
                hasVideo = true;
                isHD = true;                                            // HVEC is always HD, type 4|5|6|7 would be even UHD (see below dedicated detection function)
                type = Component->type;
            } else if (Component = Components->GetComponent(0, 5, 0)) { // #2: H.264 (stream content: 5 - recording info: "X 5 <type>"
                hasVideo = true;
                type = Component->type;
            } else if (Component = Components->GetComponent(0, 1, 0)) { // #3: MPEG2 (stream content: 1 - recording info: "X 1 <type>"
                hasVideo = true;
                type = Component->type;
            };

            if (isHD) { // stream content: 9
                switch (type) {
                    case 0x04:
                    case 0x05:
                    case 0x06:
                    case 0x07:
                        format = HD | UHD;
                        break;
                };
            } else {
                switch (type) {
                    case 0x09:
                    case 0x0A:
                    case 0x0B:
                    case 0x0C:
                    case 0x0D:
                    case 0x0E:
                    case 0x0F:
                    case 0x10:
                        format = HD;
                        isHD = true;
                        break;
                };
            };
        };
    }; // LOCK_RECORDINGS_READ

    if ((isHD == false) && (type == -1) && (!(channelID == tChannelID::InvalidID))) {
        // fallback to retrieve via channel (in case of EPG issues)
#if defined (APIVERSNUM) && (APIVERSNUM >= 20301)
        LOCK_CHANNELS_READ;
        const cChannel *channel = Channels->GetByChannelID(channelID);
#else
        const cChannel *channel = Channels.GetByChannelID(channelID);
#endif
        if (channel) {
            switch (channel->Vtype()) {
                case 0x1b: // H.264
                    format = HD;
                    hasVideo = true;
                    break;
                case 0x24: // H.265
                    format = HD | UHD;
                    hasVideo = true;
                    break;
            };
        };
    };

    // detect Radio
   if ((hasAudio == true) && (hasVideo == false)) {
        if (FramesPerSecond < 23) { // workaround for issue of missing "X 1" on some SD channels (e.g. RTL)
            format = RADIO; // isRadio
        };
    };

    return format;
};
